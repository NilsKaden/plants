package main

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var db = dynamodb.New(session.New(), aws.NewConfig().WithRegion("eu-west-1"))

func getItem(id string) (*plant, error) {
	input := &dynamodb.GetItemInput{
		TableName: aws.String("Plants"),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				N: aws.String(id),
			},
		},
	}

	result, err := db.GetItem(input)
	if err != nil {
		return nil, err
	}
	if result.Item == nil {
		return nil, nil
	}

	plant := new(plant)
	err = dynamodbattribute.UnmarshalMap(result.Item, plant)

	if err != nil {
		return nil, err
	}

	return plant, nil
}

func getItems() ([]plant, error) {
	input := &dynamodb.ScanInput{
		TableName: aws.String("Plants"),
	}

	result, err := db.Scan(input)
	if err != nil {
		return nil, err
	}

	var plants = []plant{}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &plants)
	if err != nil {
		return nil, err
	}

	return plants, nil
}

func putItem(plant *plant) error {
	input := &dynamodb.PutItemInput{
		TableName: aws.String("Plants"),
		Item: map[string]*dynamodb.AttributeValue{
			"id": {
				N: aws.String(plant.ID),
			},
			"title": {
				S: aws.String(plant.Title),
			},
			"species": {
				S: aws.String(plant.Species),
			},
			"latin": {
				S: aws.String(plant.Latin),
			},
			"description": {
				S: aws.String(plant.Description),
			},
			"author": {
				S: aws.String(plant.Author),
			},
			"authorid": {
				S: aws.String(plant.AuthorID),
			},
			"imageurl": {
				S: aws.String(plant.ImageURL),
			},
			"galleryurls": {
				SS: aws.StringSlice(plant.GalleryImageURLs),
			},
			"time": {
				S: aws.String(plant.Time),
			},
		},
	}

	_, err := db.PutItem(input)
	return err
}
