to put item to dynamo db manually:

$ aws dynamodb put-item --table-name Plants --item '{"id": {"N": "1"}, "title": { "S": "uno plant"}, "species": { "S": "species"}, "latin": {"S": "Plantus Gutus"}, "description": { "S": "Its a very good plant. I have the best plants. I know many plants."}, "author": { "S": "GardenTrump" }, "authorid": { "S": "1aaaaaaa"}, "imageurl": {"S": "https://picsum.photos/800/600"}, "galleryurls": {"SS": ["https://picsum.photos/id/1/200", "https://picsum.photos/id/2/200"]}, "time": { "S": "2020-07-23T19:26:00+0000"}}'

$ aws dynamodb put-item --table-name Plants --item '{"id": {"N": "2"}, "title": { "S": "due plant"}, "species": { "S": "species2"}, "latin": {"S": "Plantus Gigantus"}, "description": { "S": "Its a very large plant. I have the best plants. I know many plants."}, "author": { "S": "GardenTrump Jr." }, "authorid": { "S": "2bbbbbbb"}, "imageurl": {"S": "https://picsum.photos/800/600"}, "galleryurls": {"SS": ["https://picsum.photos/id/1/200", "https://picsum.photos/id/2/200"]}, "time": { "S": "2020-07-23T19:32:00+0000"}}'


CI:

$ env GOOS=linux GOARCH=amd64 go build -o ./build plants && zip -j ./build/plants.zip ./build/plants && aws lambda update-function-code --function-name plants --zip-file fileb://./build/plants.zip

some commands:
$ env GOOS=linux GOARCH=amd64 go build -o ./build plants
$ zip -j ./build/plants.zip ./build/plants
$ aws lambda update-function-code --function-name plants --zip-file fileb://./build/plants.zip
$ aws lambda invoke --function-name plants ./build/response.json 
$ aws apigateway test-invoke-method --rest-api-id 8lsqrg05ck --resource-id r9js4u --http-method "GET" --path-with-query-string "/plants?id=2"

some aws config stuff:
rest-api-id: 8lsqrg05ck
root-path-id: shpczr4659
resource-id: r9js4u
stage deployment api: n083qd

right now we are using a service-style api. Microservice-style (one lambda for each REST operation)

https://www.alexedwards.net/blog/serverless-api-with-go-and-aws-lambda

GET Example: 
curl https://8lsqrg05ck.execute-api.eu-west-1.amazonaws.com/staging/plants\?id\=1

POST Example: 
curl -i -H "Content-Type: application/json" -X POST -d '{"id":"3", "title":"Plant3", "author": "API author", "species": "api species", "latin": "plantus from da api-us", "description": "this plant is definitly not from here. it seems to be from a land far far away. api land", "authorid": "3cajFHsjb", "imageurl": "https://picsum.photos/800/600", "galleryurls": ["https://picsum.photos/id/1/200", "https://picsum.photos/id/2/200"], "time": "2020-07-23T19:59:00+0000"}' https://8lsqrg05ck.execute-api.eu-west-1.amazonaws.com/staging/plants

JS test requests: 
GET
fetch('https://8lsqrg05ck.execute-api.eu-west-1.amazonaws.com/staging/plants?id=1').then(res => res.json())

POST
fetch('https://8lsqrg05ck.execute-api.eu-west-1.amazonaws.com/staging/plants', { method: 'POST', headers: { 'Content-Type': 'application/json'}, body: JSON.stringify({"id":"3", "title":"Plant3", "author": "API author", "species": "api species", "latin": "plantus from da api-us", "description": "this plant is definitly not from here. it seems to be from a land far far away. api land", "authorid": "3cajFHsjb", "imageurl": "https://picsum.photos/800/600", "galleryurls": ["https://picsum.photos/id/1/200", "https://picsum.photos/id/2/200"], "time": "2020-07-23T19:59:00+0000"})})