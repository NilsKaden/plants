package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// numeric id
var idRegexp = regexp.MustCompile(`^[0-9]+$`)
var errorLogger = log.New(os.Stderr, "ERROR", log.Llongfile)

type plant struct {
	ID               string   `json:"id"`
	Title            string   `json:"title"`
	Species          string   `json:"species"`
	Latin            string   `json:"latin"`
	Description      string   `json:"description"`
	Author           string   `json:"author"`
	AuthorID         string   `json:"authorid"`
	ImageURL         string   `json:"imageurl"`
	GalleryImageURLs []string `json:"galleryurls"`
	Time             string   `json:"time"`
}

func router(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	switch req.HTTPMethod {
	case "GET":
		// switch between getOne and getAll
		id := req.QueryStringParameters["id"]
		if id == "" {
			return getPlants(req)
		}
		return getPlant(req)
	case "POST":
		return createPlant(req)
	case "PUT":
		return updatePlant(req)
	case "DELETE":
		return deletePlant(req)
	case "OPTIONS":
		return handleCors(req)
	default:
		return clientError(http.StatusMethodNotAllowed)
	}
}

func getPlant(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// get id from query parameter and validate
	id := req.QueryStringParameters["id"]
	if !idRegexp.MatchString(id) {
		return clientError(http.StatusBadRequest)
	}

	// fetch plant from dynamodb
	plant, err := getItem(id)
	if err != nil {
		return serverError(err)
	}
	if plant == nil {
		return clientError(http.StatusNotFound)
	}

	// The APIGatewayProxyResponse.Body field needs to be a string, so we marshal the book record into JSON.
	js, err := json.Marshal(plant)
	if err != nil {
		return serverError(err)
	}

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       string(js),
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*"},
	}, nil
}

func getPlants(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// write a DB handler to handle pagination etc.

	plants, err := getItems()

	if err != nil {
		return serverError(err)
	}

	js, err := json.Marshal(plants)
	if err != nil {
		return serverError((err))
	}

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       string(js),
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*"},
	}, nil
}

func createPlant(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if req.Headers["content-type"] != "application/json" && req.Headers["Content-Type"] != "application/json" {
		return clientError(http.StatusNotAcceptable)
	}

	plant := new(plant)
	err := json.Unmarshal([]byte(req.Body), plant)
	if err != nil {
		return clientError(http.StatusUnprocessableEntity)
	}

	if !idRegexp.MatchString(plant.ID) {
		return clientError(http.StatusBadRequest)
	}
	if plant.Title == "" || plant.Author == "" {
		return clientError(http.StatusBadRequest)
	}

	// TODO: generate ID serverside
	err = putItem(plant)
	if err != nil {
		return serverError(err)
	}

	return events.APIGatewayProxyResponse{
		StatusCode: 201,
		Headers:    map[string]string{"Location": fmt.Sprintf("/plants?id=%s", plant.ID), "Access-Control-Allow-Origin": "*"},
	}, nil
}

func updatePlant(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: 418,
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*"},
	}, nil
}

func deletePlant(events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: 418,
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*"},
	}, nil
}

func handleCors(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers:    map[string]string{"Allow": "GET, POST, OPTIONS", "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "*"},
	}, nil
}

// Add a helper for handling errors. This logs any error to os.Stderr
// and returns a 500 Internal Server Error response that the AWS API
// Gateway understands.
func serverError(err error) (events.APIGatewayProxyResponse, error) {
	errorLogger.Println(err.Error())

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Body:       http.StatusText(http.StatusInternalServerError),
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*"},
	}, nil
}

// Similarly add a helper for send responses relating to client errors.
func clientError(status int) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: status,
		Body:       http.StatusText(status),
		Headers:    map[string]string{"Access-Control-Allow-Origin": "*"},
	}, nil
}

func main() {
	lambda.Start(router)
}
